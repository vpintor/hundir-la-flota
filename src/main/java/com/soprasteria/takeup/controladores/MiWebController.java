package com.soprasteria.takeup.controladores;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.soprasteria.takeup.dto.Producto;

@Controller
public class MiWebController {
	
	
	@Value("${titulo}")
	String titulo;
	
	@GetMapping ("/inicio")
	public String init (Model model) {
		
		ArrayList<Producto> productos = new ArrayList<>();
		
		productos.add(new Producto("Agua embotellada", 10.5f));
		productos.add(new Producto("Leche", 7.5f));
		
		model.addAttribute("titulo", titulo);
		model.addAttribute("productos", productos);
		return "inicio";

	}
	
	

	

}
