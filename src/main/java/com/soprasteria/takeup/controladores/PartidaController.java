package com.soprasteria.takeup.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("partida")
public class PartidaController {

	
	@GetMapping("/elegir")
	public String elegir(){
		return "elegirOponente";
	}
	
	@GetMapping("/iniciar")
	public String iniciarPartida(){
		return "partida";
	}
}
