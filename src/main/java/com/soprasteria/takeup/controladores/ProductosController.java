package com.soprasteria.takeup.controladores;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soprasteria.takeup.dto.Producto;

@RestController
public class ProductosController {
	
	@GetMapping("/api/lista")
	public List<Producto> lista(){
		ArrayList<Producto> productos = new ArrayList<>();
		
		productos.add(new Producto("Agua embotellada", 10.5f));
		productos.add(new Producto("Leche", 7.5f));
		
		return productos;
	}
	
	
}
