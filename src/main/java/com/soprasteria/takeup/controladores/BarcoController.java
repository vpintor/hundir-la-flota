package com.soprasteria.takeup.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("barcos")
public class BarcoController {

	
	@GetMapping("/colocarBarco")
	public String colocarBarco(){
		return "colocarBarco";
	}
}
