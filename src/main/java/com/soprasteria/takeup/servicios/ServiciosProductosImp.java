package com.soprasteria.takeup.servicios;

import java.util.ArrayList;
import java.util.List;

import com.soprasteria.takeup.dto.Producto;

public class ServiciosProductosImp implements IProductosServicios{
	
	public List<Producto> lista(){
		
		ArrayList<Producto> productos = new ArrayList<>();
		productos.add(new Producto("Agua embotellada", 10.5f));
		productos.add(new Producto("Leche", 7.5f));
		
		return productos;
	}
	

}
