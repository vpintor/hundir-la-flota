package com.soprasteria.takeup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HundirlaflotaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HundirlaflotaApplication.class, args);
	}

}
